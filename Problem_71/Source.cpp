﻿
#include <limits>
#include <map>
#include <vector>
#include <algorithm>
#include <string>
#include <iostream>

const unsigned int INF = std::numeric_limits<unsigned int>::max();
typedef std::pair<int, unsigned int> pair;

struct lesser_pair_by_first {
  bool operator ()(const pair &p1, const pair &p2) {
    return p1.first < p2.first;
  }
};

unsigned int bruteforce(const std::vector<std::vector<pair>> &net, std::vector<int> &y) {
  bool finished = false;
  int vertices = net.size();
  unsigned int path_length = 0;
  std::vector<int> edge(vertices, 0);
  std::vector<int> path(vertices);
  std::vector<bool> visited(vertices, false);
  int level = 0;
  unsigned int shortest = INF;
  std::vector<int> shortest_path(vertices);
  path[level] = 0;
  visited[path[level]] = true;
  while (!finished) {
    if (edge[level] < net[path[level]].size()) {
      if (!visited[net[path[level]][edge[level]].first]) {
        path[level + 1] = net[path[level]][edge[level]].first;
        path_length += net[path[level]][edge[level]].second;
        visited[path[++level]] = true;
      }
      else {
        edge[level]++;
      }
    }
    else {
      if (level == net.size() - 1 && path_length < shortest) {
        shortest = path_length;
        std::copy(path.begin(), path.end(), shortest_path.begin());
      }
      edge[level] = 0;
      visited[path[level--]] = false;
      if (level >= 0) {
        path_length -= net[path[level]][edge[level]].second;
        edge[level]++;
      }
      else {
        finished = true;
      }
    }
  }
  y = shortest_path;
  return shortest;
}

int main() {
  std::map<std::string, int> ids { { "Bishkek", 0 } };
  std::map<std::string, int> ids2;
  std::map<int, std::string> names { { 0, "Bishkek" } };
  std::vector<std::vector<pair>> net;

  int N, M;
  unsigned int price;
  int size;

  std::cin >> N;
  size = N + 2;
  net.resize(size, std::vector<pair> { });
  std::string city, city2;
  for (int i = 1; i <= N; i++)
  {
    std::cin >> city;
    ids.insert({ city, i });
    ids2.insert({ city, i });
    names.insert({ i, city });
  }
  ids2.insert({ "Bishkek", N + 1 });
  names.insert({ N + 1, "Bishkek" });
  std::cin >> M;
  for (int i = 0; i < M; i++) {
    std::cin >> city >> city2 >> price;
    net[ids[city]].push_back(pair { ids2[city2], price });
  }
  for (int i = 0; i < size - 1; i++) {
    std::sort(net[i].begin(), net[i].end(), lesser_pair_by_first());
  }
  std::vector<int> y;
  unsigned int l = bruteforce(net, y);
  if (l != INF) {
    std::cout << l << std::endl;
    std::cout << names[y[0]];
    for (int i = 1; i < size; i++) {
      std::cout << " - " << names[y[i]];
    }
  }
  else {
    std::cout << "No solution";
  }

  return 0;
}
